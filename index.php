<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--fontawesome css-->
    <link rel="stylesheet" href="css/fontawesome.min.css">
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <!-- custom css -->
    <link rel="stylesheet" href="css/custom.css">

    <title>Online Service Management</title>
</head>

<body>
    <!--navigation-->
    <nav class="navbar navbar-expand-sm navbar-dark pl-5  bg-danger fixed-top">
        <a href="index.php" class="navbar-brand">OSMS</a>
        <span class="navbar-text">Customer's Happines is our Aim</span>
        <button type="buttton" class="navbar-toggler" data-toggle="collapse" data-target="#myMenu">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="myMenu">
            <ul class="navbar-nav pl-5 ml-auto">
                <li class="nav-item"><a href="index.php" class="nav-link text-white">Home</a></li>
                <li class="nav-item"><a href="#Services" class="nav-link text-white">Services</a></li>
                <li class="nav-item"><a href="#Registration" class="nav-link text-white">Registration</a></li>
                <li class="nav-item"><a href="#Login" class="nav-link text-white">Login</a></li>
                <li class="nav-item"><a href="#Contact" class="nav-link text-white">Contact</a></li>
            </ul>
        </div>
    </nav>
    <!-- end of navigation -->
    <!-- start header jumbotron -->
    <header class="jumbotron back-image" style="background-image: url(images/banner.jpg);  ">
        <div class="myclass main-heading">
            <h1 class="text-uppercase text-danger font-weight-bold">Welcome to OSMS</h1>
            <p class="font-italic">Customer's Happiness is our Aim</p>
            <a href="#" class="btn btn-success mr-4">Login</a>
            <a href="#" class="btn btn-danger mr-4">Sign Up</a>
        </div>
    </header>
    <!-- start introduction section  -->
    <div class="container" id="Services">
        <div class="jumbotron">
            <h3 class="text-center">OSMS Service</h3>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad ipsa eum enim, esse numquam sit at provident
                voluptates fugiat? Porro molestiae est nisi corporis reiciendis iure voluptates accusamus voluptas
                tempore blanditiis perferendis impedit minus vitae a aperiam voluptatem consequuntur, dolorum maxime?
                Beatae voluptatem itaque consectetur esse illo explicabo laudantium corporis doloremque, placeat
                deleniti provident excepturi velit inventore debitis molestiae libero dolore nemo qui et. Culpa error
                ratione temporibus eos magni doloribus porro aut esse excepturi, corrupti eveniet cum! Autem explicabo
                excepturi expedita. Dicta ab beatae laborum error vero autem excepturi aspernatur at harum veniam
                dolores quae magnam omnis, facilis necessitatibus.</p>
        </div>
    </div>
    <!-- end introduction section  -->
    <!--start services  -->
    <div class="container text-center border-bottom">
        <h2 class="text-center ml-5">Our Services</h2>
        <div class="row">
            <div class="col-md-4">
                <div class="left-side" style="font-size: 2.3rem;">
                    <span class="fas fa-tv text-success fa-5x"></span>
                    <h3 class="mt-4">Electronic Appliances</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="center-side text-center" style="font-size: 2.3rem;">
                    <span class="fas fa-sliders-h text-primary fa-5x ml-5"></span>
                    <h3 class="mt-4 ml-4">PreventiveMaintenance</h3>
                </div>
            </div>
            <div class="col-md-4">
                <div class="right-side text-center" style="font-size: 2.3rem;">
                    <span class="fas fa-cogs text-info fa-5x ml-5"></span>
                    <h3 class="mt-4 ml-4">Fault Repair</h3>
                </div>
            </div>
        </div>
    </div>
    <!--end services  -->

    <!-- start registration form -->
    <div class="container pt-5">
        <h2 class="text-center">Create an Account</h2>
        <div class="row mt-4 mb-4">
            <div class="col-md-6 offset-md-3" style="box-shadow:5px 5px 5px rgba(68,68,68,0.6);">
                <form action="" method="post" class="shadow-lg p-4">
                    <div class="form-group">
                        <i class="fas fa-user"></i> <label for="name" class="font-weight-bold pl-2">Name</label>
                        <input type="text" name="rName" class="form-control" placeholde="Name">
                    </div>
                    <div class="form-group">
                        <i class="fas fa-envelope-square"></i> <label for="email"
                            class="font-weight-bold pl-2">Email</label>
                        <input type="email" name="rEmail" class="form-control" placeholde="Email">
                        <small class="text-muted">We'll never share your email with anyone else </small>
                    </div>
                    <div class="form-group">
                        <i class="fas fa-key"></i> <label for="password" class="font-weight-bold pl-2">Password</label>
                        <input type="password" name="rPassword" class="form-control" placeholde="Password">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger mt-5 btn-block shadow-sm font-weigh-bold"
                            name="rSignup">Sign Up</button>
                        <em class="text-muted" style="font-size: 10px;">Note:- By clicking Sign Up you agree to Our Term
                            Data
                            and Cookie
                            Policy.
                        </em>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end registration form -->
    <!-- Happy customer -->
    <div id="card">
        <div class="container-fluid bg-danger p-5" style="min-height:400px;">
            <div class="container">
                <div class="text-field">
                    <h2 class="text-white text-center">Happy Customers</h2>
                </div>
                <div class="row mt-5">
                    <div class="col-md-3 shadow-lg">
                        <div class="card bg-white card-1" style="border-radius: 10px;" id="card-container">
                            <div class="card-header">
                                <div class="image-container">
                                    <img src="images/d.jpeg" alt="image"
                                        class="img-fluid w-50 h-75 ml-5 rounded-circle">
                                    <h3 class="text-center mt-2">Rahul Kumar</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="text-muted text-center">Lorem, ipsum dolor sit amet consectetur adipisicing
                                    elit.
                                    Quas perferendis dolores officia
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 shadow-lg">
                        <div class="card bg-white" style="border-radius: 10px;">
                            <div class="card-header">
                                <div class="image-container">
                                    <img src="images/a.jpeg" alt="image"
                                        class="img-fluid w-50 h-75 ml-5 rounded-circle">
                                    <h3 class="text-center mt-2">John Doe</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="text-muted text-center">Lorem, ipsum dolor sit amet consectetur adipisicing
                                    elit.
                                    Quas perferendis dolores officia
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 shadow-lg">
                        <div class="card bg-white" style="border-radius: 10px;">
                            <div class="card-header">
                                <div class="image-container">
                                    <img src="images/d.jpeg" alt="image"
                                        class="img-fluid w-50 h-75 ml-5 rounded-circle">
                                    <h3 class="text-center mt-2">Rahul Kumar</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="text-muted text-center">Lorem, ipsum dolor sit amet consectetur adipisicing
                                    elit.
                                    Quas perferendis dolores officia
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 shadow-lg">
                        <div class="card bg-white" style="border-radius: 10px;">
                            <div class="card-header">
                                <div class="image-container">
                                    <img src="images/a.jpeg" alt="image"
                                        class="img-fluid w-50 h-75 ml-5 rounded-circle">
                                    <h3 class="text-center mt-2">John Doe</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="text-muted text-center">Lorem, ipsum dolor sit amet consectetur adipisicing
                                    elit.
                                    Quas perferendis dolores officia
                                </p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- Happy customer -->
    <!-- contact page -->
    <div class="container p-5">
        <div class="row">
            <div class="col-md-8">
                <h3 class="text-center m-4">Contact Us</h3>
                <form action="" method="post">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="Name"><br>
                        <input type="text" name="subject" class="form-control" placeholder="Subject"><br>
                        <input type="email" name="email" class="form-control" placeholder="Email"><br>
                        <textarea name="message" id="message" cols="20" rows="5" class="form-control"
                            placeholder="How can You help you ?" style="resize:none;">
                        </textarea><br>
                        <button type="submit" class="btn btn-primary mt-2" name="submit">Send</button>
                    </div>
                </form>

            </div>
            <div class="col-md-4 text-center mt-5">
                <h4>Headerquarter:</h4>
                <span class="text-center text-dark">www.olizetSore.com</span><br>
                <span class="text-center text-dark"> Sec IV. Bokaro</span><br>
                <span class="text-center text-dark">Thimi,Bhaktapur - 2345</span><br>
                <span class="text-center text-dark">Phone : +977 9853323342-</span>
                <h2 class="text-center text-dark"><strong>www.osms.com</strong></h2>
                <hr><br><br>
                <h4>Kathmandu Branch:</h4>
                <span class="text-center text-dark">OSMS Pvt.Ltd.</span> <br>
                <span class="text-center text-dark">Kathmandu thimi, Delhi</span>
                <span class="text-center text-dark">Phone : +977 9853323342-</span>
                <h2 class="text-center text-dark"><strong>www.osms.com</strong></h2>
            </div>
        </div>
    </div> <!-- end contact page -->

    <!--footer in the last page -->
    <footer class="container-fluid bg-dark mt-5">
        <div class="container">
            <div class="row py-3">
                <div class="col-md-6">
                    <span class="text-white">Follow Us:</span>
                    <a href="#" class="pr-2 text-white"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="pr-2 text-white"><i class="fab fa-twitter"></i></a>
                    <a href="#" class="pr-2 text-white"><i class="fab fa-youtube"></i></a>
                    <a href="#" class="pr-2 text-white"><i class="fab fa-google-plus-g"></i></a>
                    <a href="#" class="pr-2 text-white"><i class="fas fa-rss"></i></a>
                </div>
                <div class=" col-md-6 text-right">
                    <span class="text-white">Designed By: <a href="www.olizetSore.com"
                            class="text-white">www.olizetSore.com &copy; <?php echo date('Y'); ?></a></span>
                    <small class="ml-2"><a href="#" class="text-white">Admin Login</a></small>

                </div>
            </div>

        </div>

    </footer>

    <!--footer in the last page -->
    <script src=" js/jquery.js"> </script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/fontawesome.min.js"></script>
</body>

</html>